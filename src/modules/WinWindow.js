import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../actions';

class WinWindow extends Component {
    render() {
        const styleDiv = { 
            height: `${this.props.height}px`
        }
        return (
            <div className="modal win" style={ styleDiv }>
                <h1>Поздравляем с победой!!!</h1>
                <button onClick={() => this.handleClick()}>Хочу еще раз!</button>
            </div>
        )
    }

    handleClick() {
        const { restart } = this.props;
        restart();
    }
}

export default connect((state) => ({ state }), actions)(WinWindow)