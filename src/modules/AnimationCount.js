import React, { Component } from 'react';

class AnimationCount extends Component {
    render() {
        const count = this.props.count;
        const styleDiv = { 
            height: `${this.props.height}px`
        }
        return (
            <div className="modal animation" style={styleDiv}>
                {count}
            </div>
        )
    }
}

export default AnimationCount;