import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import {
    SIZE_BLOCK, EMPTY, WALL, GOAL, BOX, HUMAN,
    LEFT, RIGHT, TOP, DOWN
} from './constants';

import emptyImg from '../images/empty.png';
import goalImg from '../images/goal.png';
import boxImg from '../images/box.png';
import wallImg from '../images/wall.png';
import hDownImg from '../images/hdown.png';
import hTopImg from '../images/htop.png';
import hRightImg from '../images/hright.png';
import hLeftImg from '../images/hleft.png';

class GameBoard extends Component {
    constructor(props) {
        super(props);
        this.widthCanvas = this.props.state.width * SIZE_BLOCK;
        this.heigthCanvas = this.props.state.height * SIZE_BLOCK;

        this.load = 0;
        this.imagesPromises = {
            [WALL]: this.loadImage(wallImg),
            [EMPTY]: this.loadImage(emptyImg),
            [GOAL]: this.loadImage(goalImg),
            [BOX]: this.loadImage(boxImg),
            [HUMAN + DOWN]: this.loadImage(hDownImg),
            [HUMAN + TOP]: this.loadImage(hTopImg),
            [HUMAN + LEFT]: this.loadImage(hLeftImg),
            [HUMAN + RIGHT]: this.loadImage(hRightImg)
        }
    }

    loadImage(url){
        return new Promise((resolve, reject) => {
            let img = new Image();
            img.addEventListener('load', e => resolve(img));
            img.addEventListener('error', () => {
                reject(new Error(`Failed to load image's URL: ${url}`));
            });
            img.src = url;
        });
    }

    render() {
        return (
            <div className="game-board">
                <canvas ref="canvas"
                    width={this.widthCanvas} height={this.heigthCanvas}></canvas>
            </div>
        )
    }

    componentDidUpdate() {
        this.updateCanvas()
    }

    componentDidMount() {
        this.updateCanvas();
    }

    updateCanvas() {
        const ctx = this.refs.canvas.getContext('2d');

        const { state } = this.props;

        for (let y = 0; y < state.height; y++) {
            for (let x = 0; x < state.width; x++) {
                this.imagesPromises[state.board[y][x]].then(img => {
                    ctx.drawImage(img, x * SIZE_BLOCK, y * SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK)
                })
            }
        }

        for (let i = 0; i < state.boxCount; i++) {
            let [y, x] = state.goals[i];
            this.imagesPromises[GOAL].then(img => {
                ctx.drawImage(img, x * SIZE_BLOCK, y * SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK)
            })
        }

        for (let i = 0; i < state.boxCount; i++) {
            let [y, x] = state.boxes[i];
            this.imagesPromises[BOX].then(img => {
                ctx.drawImage(img, x * SIZE_BLOCK, y * SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK)
            })
        }

        this.imagesPromises[HUMAN + state.course].then(img => {
            ctx.drawImage(img, state.humanCoord[1] * SIZE_BLOCK, state.humanCoord[0] * SIZE_BLOCK, SIZE_BLOCK, SIZE_BLOCK)
        })

    }

}

export default connect(state => ({ state }), actions)(GameBoard);