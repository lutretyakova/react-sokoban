import {
    HUMAN, EMPTY, WALL, BOX,
    RIGHT, LEFT, TOP, DOWN, GOAL
} from './modules/constants';
import { CHANGE_COURSE, RESTART } from './actions';
import * as gameMap from './map.json';

export default (state = init(), actions) => {
    switch (actions.type) {
        case CHANGE_COURSE:
            return changeCourse(state, actions.keyCode);
        case RESTART:
            return init();
        default:
            return state
    }
}

const getCoords = (map, obj, width) => {
    const reg = new RegExp(obj, "g");
    const objPos = map.replace(/\s|,/g, "")
        .search(reg);
    const objCoord = [
        Math.floor(objPos / width),
        objPos % width
    ]

    return objCoord
}


const init = () => {
    const width = parseInt(gameMap.width, 10);
    const height = parseInt(gameMap.height, 10);

    const humanCoord = getCoords(gameMap.board, HUMAN, width);
    const humanReg = new RegExp(HUMAN, "g");
    let mapLine = gameMap.board
        .replace(/\s/g, '')
        .replace(humanReg, EMPTY);

    const boxCount = parseInt(gameMap.boxCount, 10);
    const boxReg = new RegExp(BOX);
    let boxes = [];
    for (let i = 0; i < boxCount; i++) {
        boxes.push(getCoords(mapLine, BOX, width));
        mapLine = mapLine.replace(boxReg, EMPTY);
    }

    const goalReg = new RegExp(GOAL);
    let goals = [];
    for (let i = 0; i < boxCount; i++) {
        goals.push(getCoords(mapLine, GOAL, width));
        mapLine = mapLine.replace(goalReg, EMPTY);
    }

    mapLine = mapLine
        .split(',')
        .map(elem => parseInt(elem, 10));

    let board = (new Array(height)).fill(null)
        .map((_, inx) => (
            mapLine.slice(inx * width, (inx + 1) * width)
        ));

    return {
        board,
        width,
        height,
        boxCount,
        humanCoord,
        boxes,
        goals,
        course: DOWN,
        count: 0
    }
}


const changeCourse = (state, keyCode) => {
    let course = keyCode;

    let [dy, dx] = [0, 0];
    switch (course) {
        case TOP:
            dy = -1;
            break;
        case RIGHT:
            dx = 1;
            break;
        case LEFT:
            dx = -1;
            break;
        case DOWN:
            dy = 1;
            break;
        default:
            return state
    }

    const newHumanCoord = [
        state.humanCoord[0] + dy,
        state.humanCoord[1] + dx
    ]

    let boxes = [];
    if (isInsideBoard(newHumanCoord, state.height, state.width) &&
        WALL !== state.board[newHumanCoord[0]][newHumanCoord[1]]) {
        if (!isCoordEquals(state.boxes, newHumanCoord)) {
            return {
                ...state,
                course,
                humanCoord: newHumanCoord,
                isShowAnimation: false
            }
        } else {
            const newBoxCoord = [
                newHumanCoord[0] + dy,
                newHumanCoord[1] + dx
            ];

            if (isInsideBoard(newBoxCoord, state.height, state.width) &&
                WALL !== state.board[newBoxCoord[0]][newBoxCoord[1]] &&
                !isCoordEquals(state.boxes, newBoxCoord)) {
                boxes = state.boxes.filter((item) => (
                    !(item[0] === newHumanCoord[0] && item[1] === newHumanCoord[1]))
                )
                boxes.push(newBoxCoord);

                const count = state.goals.filter(
                    (item) => (isCoordEquals(boxes, item))
                ).length;

                const isShowAnimation = count > state.count && count !== state.boxCount ? true : false;
                return {
                    ...state,
                    humanCoord: newHumanCoord,
                    course,
                    boxes,
                    count,
                    isShowAnimation
                }
            }
        }

    }

    return {
        ...state,
        course,
        isShowAnimation: false
    }
}


const isCoordEquals = (coordsArr, coord) => {
    return coordsArr
        .filter((item) => (item[0] === coord[0] && item[1] === coord[1]))
        .length > 0
}


const isInsideBoard = (coord, height, width) => {
    return coord[0] >= 0 && coord[0] < height &&
        coord[1] >= 0 && coord[1] < width
}