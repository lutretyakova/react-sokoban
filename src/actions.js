export const CHANGE_COURSE = 'changeCourse';

export const RESTART = 'restart';

export const changeCourse = (keyCode) => ({
    type: CHANGE_COURSE,
    keyCode
});

export const restart = () => ({
    type: RESTART
});
