import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from './actions';

import './Sokoban.css';
import GameBoard from './modules/GameBoard';
import AnimationCount from './modules/AnimationCount';
import WinWindow from './modules/WinWindow';

import {
    LEFT, RIGHT, TOP, DOWN, SIZE_BLOCK
} from './modules/constants';

class Sokoban extends Component {
    render() {
        const { state } = this.props;
        const height = state.height * SIZE_BLOCK;
        const animation = state.isShowAnimation ? <AnimationCount count={state.count} height={height} /> : null;
        const winWindow = state.count === state.boxCount ? <WinWindow height={height}/> : null;

        return (
            <div className="page">
                <div className="page-header">
                    <header>
                        <h1 className="title">Sokoban</h1>
                        <div className="counter">{ state.count } / { state.boxCount }</div>
                    </header>
                </div>
                <GameBoard />
                {animation}
                {winWindow}
                <footer>
                    <a href="https://gitlab.com/lutretyakova/react-sokoban">https://gitlab.com/lutretyakova/react-sokoban</a>
                </footer>
            </div>
        );
    }

    componentDidMount(){
        document.addEventListener("keydown", this.handleKeyDown.bind(this));
    }

    componentWillUnmount(){
        document.removeEventListener("keydown", this.handleKeyDown.bind(this));
    }

    handleKeyDown(event){
        if([DOWN, TOP, LEFT, RIGHT].includes(event.keyCode)){
            const { changeCourse } = this.props;
            changeCourse(event.keyCode);
        }
    }


}

export default connect(state => ({ state }), actions)(Sokoban);
